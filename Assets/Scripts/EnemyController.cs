﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {
    [SerializeField]         int        health   = 100;
    [SerializeField]         float      fireRate = 1f;
    [SerializeField]         float      speed    = 1f;
    [SerializeField]         Transform  shootPosition;
    [SerializeField]         GameObject bullet;
    [SerializeField]         float      followDistance = 2f;
    [SerializeField]         float      shootDistance  = 1f;
    [SerializeField] private Transform  shootPoint;


    List<Transform> waypoints;

    int currentWaypointIndex;

    private PatrolMoving _patrolMoving;
    PlayerController     player;
    EnemyState           currentState;
    Coroutine            shootingCoroutine;

    Rigidbody2D     rigidbody;
    Animator        animator;
    private float   dist;
    private Vector2 direction;

    enum EnemyState {
        STAND,
        MOVE,
        SHOOT
    }

    // Start is called before the first frame update
    void Start() {
        rigidbody     = GetComponent<Rigidbody2D>();
        animator      = GetComponentInChildren<Animator>();
        _patrolMoving = FindObjectOfType<PatrolMoving>();
        // transform.position   = waypoints[0].position;
        // currentWaypointIndex = 1;

        player       = FindObjectOfType<PlayerController>();
        currentState = EnemyState.STAND;
    }

    // Update is called once per frame
    void Update() {
        CheckDistance();
        //if(currentState == EnemyState.STAND)
        //{
        //    //do STAND
        //}
        //else if(currentState == EnemyState.MOVE)
        //{
        //    //do MOVE
        //}
        //else if(currentState == EnemyState.SHOOT)
        //{
        //    //do SHOOT
        //}
        switch(currentState) {
            case EnemyState.STAND :
                //do STAND;
                DoStand();
                print("жду");
                break;
            case EnemyState.MOVE :
                //do MOVE;
                DoMove();
                print("иду");
                _patrolMoving.CurrentState = StateEnemy.WAIT;
                break;
            case EnemyState.SHOOT :
                //do SHOOT;
                DoShoot();
                print("стреляю");
                break;
            default :
                //do default action
                break;
        }
    }

    private void DoStand() {
        float distance = Vector2.Distance(transform.position, player.transform.position);
        if(distance <= followDistance) {
            currentState = EnemyState.MOVE;
            animator.SetTrigger("Move");
        }
    }

    private void DoMove() {
        Move();
        Rotate();
        float distance = Vector2.Distance(transform.position, player.transform.position);
        if(distance <= shootDistance) {
            currentState = EnemyState.SHOOT;
            animator.SetTrigger("Shoot");
            StopMovement();
        }
    }

    private void DoShoot() {
        Rotate();
        Shoot();

        float distance = Vector2.Distance(transform.position, player.transform.position);
        if(distance > shootDistance) {
            currentState = EnemyState.MOVE;
            animator.SetTrigger("Move");
            StopCoroutine(shootingCoroutine);
            shootingCoroutine = null;
        }
    }

    private void Rotate() {
        //Vector substraction
        direction    = player.transform.position - transform.position;
        transform.up = direction;
    }

    private void Move() {
        direction          = player.transform.position - transform.position;
        rigidbody.velocity = direction.normalized * speed;
    }

    private void StopMovement() {
        rigidbody.velocity = Vector2.zero;
    }

    private void Shoot() {
        if(shootingCoroutine == null) {
            shootingCoroutine = StartCoroutine(Shooting());
        }
    }

    IEnumerator Shooting() {
        while(true) {
            Instantiate(bullet, shootPosition.position, transform.rotation);
            yield return new WaitForSeconds(fireRate);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        DamageDealer damageDealer = collision.GetComponent<DamageDealer>();
        if(damageDealer != null) {
            health -= damageDealer.GetDamage();
        }
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, followDistance);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, shootDistance);
    }

    private void CheckDistance() {
        dist = Vector2.Distance(player.transform.position, transform.position);
    }

    private void CheckHit() {
        Vector2      _shooting = shootPoint.position;
        RaycastHit2D rayhit    = Physics2D.Raycast(_shooting, direction, 5f);

        if(rayhit.collider == null) {
            return;
        }

        if(!rayhit.collider.CompareTag("player")) {
            return;
        }
    }
}
